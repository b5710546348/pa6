package FinancialCalculator;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoanCalculatorUI {
	private JTextField loan_input,interest_input,month_input,payment_result;
	private JLabel lblMonths,lblPerYear,lblPayment,lblPerMonth,lblWarningLabel;
	private JButton reset_btn,calculate_btn;
	
	public void LoanCalculator(JPanel pane_LoanCalculator){
		JLabel lblLoanAmount = new JLabel("LOAN AMOUNT");
		lblLoanAmount.setBounds(10, 23, 120, 14);
		pane_LoanCalculator.add(lblLoanAmount);

		loan_input = new JTextField();
		loan_input.setBounds(140, 20, 95, 20);
		pane_LoanCalculator.add(loan_input);
		loan_input.setColumns(10);

		interest_input = new JTextField();
		interest_input.setBounds(140, 56, 46, 20);
		pane_LoanCalculator.add(interest_input);
		interest_input.setColumns(10);

		JLabel lblInterestRate = new JLabel("INTEREST RATE");
		lblInterestRate.setBounds(10, 59, 120, 14);
		pane_LoanCalculator.add(lblInterestRate);

		lblMonths = new JLabel("MONTHS");
		lblMonths.setBounds(10, 100, 120, 14);
		pane_LoanCalculator.add(lblMonths);

		month_input = new JTextField();
		month_input.setBounds(140, 97, 46, 20);
		pane_LoanCalculator.add(month_input);
		month_input.setColumns(10);

		lblPerYear = new JLabel("% PER YEAR");
		lblPerYear.setBounds(196, 59, 125, 14);
		pane_LoanCalculator.add(lblPerYear);

		calculate_btn = new JButton("Calculate");
		calculate_btn.setForeground(Color.BLUE);
		calculate_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Double loan_amount = Double.parseDouble(loan_input
							.getText());
					Double interest = Double.parseDouble(interest_input
							.getText());
					int month = (int) Math.floor(Integer.parseInt(month_input
							.getText()));
					LoanCalculator l = new LoanCalculator(loan_amount,interest, month);
					payment_result.setText(String.format("%.2f", Math.ceil(l.calculate())));
					lblWarningLabel.setText("");
				} catch (Exception e) {
					lblWarningLabel
							.setText("Please input all number information");
					lblWarningLabel.setForeground(Color.RED);
				}
			}
		});
		calculate_btn.setBounds(138, 153, 95, 36);
		pane_LoanCalculator.add(calculate_btn);

		payment_result = new JTextField();
		payment_result.setBounds(140, 205, 86, 20);
		pane_LoanCalculator.add(payment_result);
		payment_result.setColumns(10);
		payment_result.setEditable(false);

		lblPayment = new JLabel("PAYMENT");
		lblPayment.setForeground(Color.BLUE);
		lblPayment.setBounds(10, 205, 120, 14);
		pane_LoanCalculator.add(lblPayment);

		lblPerMonth = new JLabel("PER MONTH");
		lblPerMonth.setForeground(Color.BLUE);
		lblPerMonth.setBounds(235, 205, 68, 14);
		pane_LoanCalculator.add(lblPerMonth);

		lblWarningLabel = new JLabel("");
		lblWarningLabel.setBounds(60, 128, 214, 14);
		pane_LoanCalculator.add(lblWarningLabel);

		reset_btn = new JButton("reset");
		reset_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				loan_input.setText("");
				interest_input.setText("");
				month_input.setText("");
				payment_result.setText("");
			}
		});
		reset_btn.setBounds(140, 249, 73, 23);
		pane_LoanCalculator.add(reset_btn);
	}
}
