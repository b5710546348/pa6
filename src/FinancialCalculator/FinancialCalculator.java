package FinancialCalculator;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 2 May 2015
 */
public interface FinancialCalculator {
	public double calculate();
}
