package FinancialCalculator;
import java.util.Date;
/**
 * This class is model of Loan Calculator UI.
 * @author Patchara Pattiyathanee 5710546348
 * @version 29 April 2015
 */
public class LoanCalculator implements FinancialCalculator{
	private int month;
	private double interest_rate;
	private double loan_amount;
	private Date start_date;
	
	public LoanCalculator(double la , double interest , int month ){
		this.loan_amount = la;
		this.interest_rate = interest;
		this.month = month;
		
	}
	
	public double calculate() {
		double I = (this.interest_rate/1200);
		double V = Math.pow(1/(1+I), this.month);
		return (I/(1-V))*this.loan_amount;
	}
	
}
