package SmartSavingMoney;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 2 May 2015
 */
public class Expenses {
	private double amount;
	private String type;
	private static double total_expenses = 0;
	private IEMemory iem ;
	private Date date = new Date();
	
	public Expenses( IEMemory iem ,double amount , String type){
		this.iem = iem;
		this.type = type;
		this.amount = amount;
		this.total_expenses += amount;
		this.date = date;
		iem.setActiveMoney(iem.getActiveMoney() - this.amount);
		anotherOption(type);
	}
	
	public double getTotalExpenses(){
		return this.total_expenses;
	}
	
	public double getExpensesAmount(){
		return this.amount;
	}
	
	public String getType(){
		return this.type;
	}
	 public void anotherOption( String type){
		 if(type.equals("Deposit Money")){
			 iem.setSavingDeposit(iem.getSavingDeposit() + this.amount);
		 }
	 }
	 public void setAmount(double amount){
		 this.amount = amount;
	 }
	 
	 public Date getDate(){
			return this.date;
	}
}
