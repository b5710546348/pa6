package SmartSavingMoney;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 6 May 2015
 */
public class IEManager implements Serializable {

	
	private JTextField value_textfield;
	private JTextArea txtrIncomeExpenses;
	private IEMemory iem;
	
	public void IEManager(JPanel IEMemory_panel , IEMemory iem){
		
		this.iem = iem;
		
		JComboBox choice = new JComboBox();
		choice.setBounds(40, 470, 100, 20);
		IEMemory_panel.add(choice);
		choice.addItem("EXPENSES"); choice.addItem("INCOME");
		
		JButton summary_btn = new JButton("Summary");
		summary_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(choice.getSelectedItem().toString().equals("EXPENSES")){
					Summarizer s = new Summarizer("EXPENSES" , "Pie chart Test", "Expenses");
					s.pack();
					s.setLocation(100, 100);
					s.setVisible(true);
				}
				else if(choice.getSelectedItem().toString().equals("INCOME")){
					Summarizer s = new Summarizer("INCOME" , "Pie chart Test", "Income");
					s.pack();
					s.setLocation(100, 100);
					s.setVisible(true);
				}
			}
		});
		summary_btn.setBounds(148, 459, 159, 32);
		IEMemory_panel.add(summary_btn);
		summary_btn.setForeground(new Color(0, 0, 128));
		summary_btn.setFont(new Font("Cordia New", Font.PLAIN, 29));
		
		txtrIncomeExpenses = new JTextArea();
		txtrIncomeExpenses.setEditable(false);
		txtrIncomeExpenses.setBounds(10, 115, 430, 333);
		IEMemory_panel.add(txtrIncomeExpenses);
		
		JScrollPane messagePane = new JScrollPane(txtrIncomeExpenses , 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		messagePane.setAutoscrolls(true);
		messagePane.setBounds(10, 115, 430, 333);
		messagePane.setAutoscrolls(true);
		IEMemory_panel.add(messagePane);
		
		
		//writeOldFile();
		
		//combo box
		JComboBox choice_comboBox = new JComboBox();
		choice_comboBox.setBounds(201, 22, 158, 23);
		IEMemory_panel.add(choice_comboBox);
		JComboBox type_comboBox = new JComboBox();
		type_comboBox.setBounds(84, 22, 88, 23);
		IEMemory_panel.add(type_comboBox);
		type_comboBox.addItem("EXPENSES");
		type_comboBox.addItem("INCOME");
		for(String s : iem.getExpensesTypesList()){
			choice_comboBox.addItem(s);
		}
		type_comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(type_comboBox.getSelectedItem().toString().equals("INCOME")){
					choice_comboBox.removeAllItems();
					for(String s : iem.getIncomeTypesList()){
						choice_comboBox.addItem(s);
					}
				}
				if(type_comboBox.getSelectedItem().toString().equals("EXPENSES")){
					choice_comboBox.removeAllItems();
					for(String s : iem.getExpensesTypesList()){
						choice_comboBox.addItem(s);
					}
				}
			}
		});
		
		//input value
		value_textfield = new JTextField();
		value_textfield.setBounds(84, 63, 109, 20);
		IEMemory_panel.add(value_textfield);
		value_textfield.setColumns(10);
		
		JLabel lblBaht = new JLabel("BAHT");
		lblBaht.setBounds(201, 66, 56, 14);
		IEMemory_panel.add(lblBaht);
		
		JButton memory_btn = new JButton("MEMORY");
		memory_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Date date = new Date();
				try{
					if(type_comboBox.getSelectedItem().toString().equals("INCOME")){
						String type = choice_comboBox.getSelectedItem().toString();
						double value = Double.parseDouble(value_textfield.getText());
						iem.incomeOperation(value, type);
						IncomeAnalyzer ia = new IncomeAnalyzer();
						
						String s = (String.format("\n%s    %s   %-20s      %.2f ",
								date.toLocaleString() , "Income" , type , value ));
						txtrIncomeExpenses.append(s);
						
						value_textfield.setText("");
					}
					if(type_comboBox.getSelectedItem().toString().equals("EXPENSES")){
						String type = choice_comboBox.getSelectedItem().toString();
						Double value = Double.parseDouble(value_textfield.getText());
						iem.expensesOperation(value, type);
						ExpensesAnalyzer ea = new ExpensesAnalyzer();
					
						String s = (String.format("\n%s    %s   %-20s      %.2f ", 
								date.toLocaleString() , "Expenses" , type , value ));
						txtrIncomeExpenses.append(s);
						
						value_textfield.setText("");
					}
				}catch(Exception ee){
					
				}
			}
		});
		memory_btn.setBounds(249, 57, 109, 32);
		IEMemory_panel.add(memory_btn);
		
		
	}
	
	
}
