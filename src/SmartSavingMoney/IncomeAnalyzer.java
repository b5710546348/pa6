package SmartSavingMoney;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 6 May 2015
 */
public class IncomeAnalyzer {
	private String status;
	private IEMemory iem = IEMemory.getInstance();
	private List<Income> classified_income  = new ArrayList<Income>();
	private static boolean isWarning = false;
	
	public IncomeAnalyzer(){
		cloneList();
		listClassification();
	}
	
	public void reset(){
		classified_income.clear();
	}
	
	public void listClassification(){
		for(int j = 0 ; j < classified_income.size() ; j++){
			for(int i = 0 ; i < iem.getIncomeList().size() ; i++){
				if(classified_income.get(j).getType().equals(iem.getIncomeList().get(i).getType())){
					double a = classified_income.get(j).getIncomeAmount() ;
					a += iem.getIncomeList().get(i).getIncomeAmount();
					classified_income.get(j).setAmount(a);
				}
			}
		}
	}
	private void cloneList(){
		for(String s : iem.getIncomeTypesList()){
			Income in = new Income(iem , 0, s);
			classified_income.add(in);
		}
	}
	
	public List<Income> getClassifiedIncomeList(){
		return this.classified_income;
	}
	
	public Income searchByName(String type){
		for(Income i : classified_income){
			if(i.getType().equals(type)){
				return i;
			}
		}
		return null;
	}
	
}
