package SmartSavingMoney;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class History {

	private JFrame frame1;
	private IEMemory iem = IEMemory.getInstance();

	/**
	 * Launch the application.
	 */
	
	public void run() {
			try {
				History window = new History();
				window.frame1.setVisible(true);
			} catch (Exception e) {
					
			}
	}
	/**
	 * Create the application.
	 */
	public History() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame1 = new JFrame();
		frame1.setBounds(100, 100, 452, 345);
		frame1.setResizable(false);
		frame1.getContentPane().setLayout(null);
		frame1.setTitle("History");
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(0, 0, 444, 271);
		textArea.setEditable(false);
		textArea.setText("");
		
		JScrollPane messagePane = new JScrollPane(textArea , 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		messagePane.setAutoscrolls(true);
		messagePane.setBounds(0, 0, 444, 271);
		messagePane.setAutoscrolls(true);
		
		textArea.append(iem.readHistory());
		
		frame1.getContentPane().add(textArea);
		frame1.getContentPane().add(messagePane);
		
		JButton btnClear = new JButton("CLEAR");
		btnClear.setBounds(339, 282, 97, 23);
		btnClear.addActionListener(p -> {
			textArea.setText("");
			iem.initializeReset();
		});
		frame1.getContentPane().add(btnClear);
		
		
	}
}
