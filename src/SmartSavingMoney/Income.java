package SmartSavingMoney;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import SmartSavingMoney.*;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 2 May 2015
 */
public class Income {
	private double amount;
	private String type;
	private static double total_income = 0;
	private IEMemory iem;
	private Date date  = new Date();
	
	public Income(IEMemory iem ,double amount ,String type){
		this.iem = iem;
		this.type = type;
		this.amount = amount;
		this.total_income += amount;
		
		iem.setActiveMoney(iem.getActiveMoney() + this.amount);
		anotherOption(type);
	}
	
	public String getType(){
		return this.type;
	}
	
	public double getTotalIncome(){
		return this.total_income;
	}
	
	public double getIncomeAmount(){
		return this.amount;
	}
	
	public void anotherOption(String type){
		if(type.equals("Withdraw Money")){
			 iem.setSavingDeposit(iem.getSavingDeposit() - this.amount);
		 }
	}
	
	public void setAmount(double income){
		this.amount = income;
	}
	
	public Date getDate(){
		return this.date;
	}
	
	
}
