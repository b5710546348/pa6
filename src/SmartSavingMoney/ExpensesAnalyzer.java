package SmartSavingMoney;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 6 May 2015
 */
public class ExpensesAnalyzer{
	private String status;
	private IEMemory iem = IEMemory.getInstance();
	private List<Expenses> classified_expenses  = new ArrayList<Expenses>();
	private static boolean isWarning = false;
	
	public ExpensesAnalyzer(){
		cloneList();
		listClassification();
	}
	
	public void reset(){
		classified_expenses.clear();
	}
	
	public void listClassification(){
		for(int j = 0 ; j < classified_expenses.size() ; j++){
			for(int i = 0 ; i < iem.getExpensesList().size() ; i++){
				if(classified_expenses.get(j).getType().equals(iem.getExpensesList().get(i).getType())){
					double a = classified_expenses.get(j).getExpensesAmount() ;
					a += iem.getExpensesList().get(i).getExpensesAmount();
					classified_expenses.get(j).setAmount(a);
				}
			}
		}
	}
	private void cloneList(){
		for(String g : iem.getExpensesTypesList()){
			Expenses e = new Expenses(iem, 0, g);
			classified_expenses.add(e);
		}
	}
	
	public List<Expenses> getClassifiedExpensesList(){
		return this.classified_expenses;
	}
	
	public Expenses searchByName(String type){
		for(Expenses e : classified_expenses){
			if(type.equals(e.getType())){
				return e;
			}
		}
		 
		return null;
	}
	
	
}
