package SmartSavingMoney;

import java.awt.EventQueue;

import FinancialCalculator.*;

import javax.swing.*;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.URL;
import java.util.Date;


public class SmartSavinMoneyApplication implements Serializable{

	private JFrame frame;
	private IEMemory iem = IEMemory.getInstance();
	private JTabbedPane tabbedPane;
	private JPanel pane_LoanCalculator;
	private ClassLoader cls = this.getClass().getClassLoader();
	

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SmartSavinMoneyApplication window = new SmartSavinMoneyApplication();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the application.
	 */
	public SmartSavinMoneyApplication() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		URL main = cls.getResource("images/MainBackground.jpg");
		ImageIcon mainIcon = new ImageIcon(main);
		JLabel mainBG = new JLabel(mainIcon);
		mainBG.setBounds(0, 0, 800, 600);
		
		frame = new JFrame();
		frame.setForeground(Color.BLACK);
		frame.setResizable(false);
		frame.setTitle("SMART SAVING MONEY V.1");
		frame.setBounds(50, 50, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setUndecorated(true);
		
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 10, 571, 530);
		frame.getContentPane().add(tabbedPane);
		
		URL exit = cls.getResource("images/exitButton.png");
		ImageIcon exitIcon = new ImageIcon(exit);
		JButton btnExit = new JButton(exitIcon);
		btnExit.setContentAreaFilled(false);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iem.writeHistory();
				frame.dispose();
			}
		});
		btnExit.setBounds(684, 10, 100,100);
		frame.getContentPane().add(btnExit);
		
		URL calURL = cls.getResource("images/CalculatorButton.png");
		ImageIcon calIcon = new ImageIcon(calURL);
		JButton btnCalculator = new JButton(calIcon);
		btnCalculator.setContentAreaFilled(false);
		
		btnCalculator.addActionListener(p -> {
			try{
				Runtime.getRuntime().exec("calc.exe");
			}catch(Exception e){}
		});
		btnCalculator.setBounds(591, 427, 100, 100);
		frame.getContentPane().add(btnCalculator);
		
		IEManager();
		FinancialCalculator();
		
		
		URL historyURL = cls.getResource("images/historyButton.png");
		ImageIcon history = new ImageIcon(historyURL);
		JButton btnHistory = new JButton(history);
		btnHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				History h = new History();
				h.run();
			}
		});
		btnHistory.setBounds(591, 316, 100, 100);
		btnHistory.setContentAreaFilled(false);
		frame.getContentPane().add(btnHistory);
		
		frame.getContentPane().add(mainBG);
	}
	
	///////////////////////////////////////////// IE MANAGER //////////////////////////////////////////////////////////////////

	private void IEManager(){
		JPanel IEMemory_panel = new JPanel();
		tabbedPane.addTab("Income and Expenses Manage", null, IEMemory_panel, null);
		IEMemory_panel.setLayout(null);
		IEManager iemg = new IEManager();
		iemg.IEManager(IEMemory_panel,iem);
	}
	
	/////////////////////////////////////////// FINANCIAL CALCULATOR /////////////////////////////////////////////////////////
	
	private JTabbedPane calculator_selection;
	
	private void FinancialCalculator(){
		JPanel FinancialCalculator_panel = new JPanel();
		tabbedPane.addTab("Financial Calculator", null, FinancialCalculator_panel, null);
		FinancialCalculator_panel.setLayout(null);
		
		JTabbedPane calculator_selection= new JTabbedPane(JTabbedPane.TOP);
		calculator_selection.setBounds(0, 11, 440, 480);
		FinancialCalculator_panel.add(calculator_selection);
		
		pane_LoanCalculator = new JPanel();
		calculator_selection.addTab("Loan Calculator", null, pane_LoanCalculator, null);
		pane_LoanCalculator.setLayout(null);
		
		LoanCalculatorUI lcui  = new LoanCalculatorUI();
		lcui.LoanCalculator(pane_LoanCalculator);
	}
}
