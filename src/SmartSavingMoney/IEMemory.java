package SmartSavingMoney;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 
 * @author Patchara Pattiyathanee 5710546348
 * @version 2 May 2015
 */
public class IEMemory implements Serializable{
	public double active_money = 0 ;
	private double saving_deposit = 0 ;
	private String type;
	private List<Expenses> IEStorage_Expenses = new ArrayList<Expenses>();
	private List<Income>   IEStorage_Income   = new ArrayList<Income>();
	private List<String> expenses_types = new ArrayList<String>();
	private List<String> income_types   = new ArrayList<String>();
	private static final IEMemory instance = new IEMemory();
	private Date date = new Date();
	private URL url = getClass().getResource("data.txt");
	private File file = new File(url.getPath());
	
	private IEMemory(){
		initTypes();
		
	}
	public static IEMemory getInstance(){
		return instance;
	}
	public void resetEachTime(){
		IEStorage_Expenses.clear();
		IEStorage_Income.clear();
	}
	public void initializeReset(){
		IEStorage_Expenses.clear();
		IEStorage_Income.clear();
		this.active_money = 0;
		this.saving_deposit = 0;
		file.delete();
	}
	public void setActiveMoney(double ActiveMoney) {
		this.active_money = ActiveMoney;
	}
	public double getActiveMoney(){
		return this.active_money;
	}
	public double getSavingDeposit(){
		return this.saving_deposit;
	}
	public void setSavingDeposit(double sd) {
		this.saving_deposit = sd;
	}
	public void incomeOperation(double amount , String type) {
		Income i = new Income( instance , amount, type);
		this.IEStorage_Income.add(i);
		
	}
	public void expensesOperation(double amount , String type) {
		Expenses e = new Expenses( instance , amount, type);
		this.IEStorage_Expenses.add(e);
		
	}
	
	public List<Expenses> getExpensesList(){
		return this.IEStorage_Expenses;
	}
	
	public List<Income> getIncomeList(){
		return this.IEStorage_Income;
	}
	public List<String> getExpensesTypesList(){
		return this.expenses_types;
	}
	
	public List<String> getIncomeTypesList(){
		return this.income_types;
	}
	
	private void initTypes(){
		expenses_types.add("Food");
		expenses_types.add("Travel Expenses");
		expenses_types.add("Consumer Product");
		expenses_types.add("Installment");
		expenses_types.add("Deposit Money");
		expenses_types.add("Other");
		income_types.add("Salary");
		income_types.add("Withdraw Money");
		income_types.add("Loan");
		income_types.add("Passive Income");
		income_types.add("Other");
	}
	
	public void writeHistory(){
		IncomeAnalyzer ia = new IncomeAnalyzer();
		ExpensesAnalyzer ea = new ExpensesAnalyzer();
		try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(date.toLocaleString()+"\n");
            bufferedWriter.write("<<Expenses>>\n");
            for(Expenses e : ea.getClassifiedExpensesList()){
            	bufferedWriter.write(e.getType() + " : " +e.getExpensesAmount()+"\n");
            	
            }
            bufferedWriter.write("<<Income>>\n");
            for(Income i : ia.getClassifiedIncomeList()){
            	bufferedWriter.write(i.getType() + " : " +i.getIncomeAmount()+"\n");
            	
            }
            bufferedWriter.close();
        }
        catch(IOException ex) {
            
        }
    }
		
		
	public String readHistory(){
		String line = null;

        try {
            
            FileReader fileReader =  new FileReader(file);
            BufferedReader bufferedReader =  new BufferedReader(fileReader);
            StringBuilder sb = new StringBuilder();
            while((line = bufferedReader.readLine()) != null) {
               
                sb.append(line+"\n");
            }    
            bufferedReader.close();
            return sb+"";
        }
        catch(FileNotFoundException ex) {
                    
        }
        catch(IOException ex) {
          
        }
        return null;
	}
	
	
}
