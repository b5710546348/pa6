package SmartSavingMoney;

import java.util.List;

import javax.swing.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;
/**
 * This class use framework to create piechart due to information of expenses.
 * @author Patchara Pattiyathanee 5710546348
 * @version 3 May 2015
 */
public class Summarizer extends JFrame{
	private ExpensesAnalyzer e = new ExpensesAnalyzer();
	private IncomeAnalyzer i = new IncomeAnalyzer();
	
	public Summarizer(String type , String appTitle , String chartTitle) {
		PieDataset dataset = createDataset(type);
		JFreeChart chart = createChart(dataset , chartTitle);
		ChartPanel chartPanel =  new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(500,300));
		setContentPane(chartPanel);
	}
	
	private PieDataset createDataset(String type){
		List<Expenses> list_expenses = e.getClassifiedExpensesList();
		List<Income> list_income = i.getClassifiedIncomeList();
		DefaultPieDataset result = new DefaultPieDataset();
		
		if(type.equals("EXPENSES")){
			for(Expenses expenses : list_expenses){
				result.setValue(expenses.getType() +" = "+ expenses.getExpensesAmount()+"", 
						expenses.getExpensesAmount()/expenses.getTotalExpenses());
			}
		}
		else if(type.equals("INCOME")){
			for(Income inc : list_income){
				result.setValue(inc.getType() +" = "+inc.getIncomeAmount()+"", 
						inc.getIncomeAmount()/inc.getTotalIncome());
			}
		}
		
		return result;
	}
	
	private JFreeChart createChart(PieDataset dataset , String title){
		JFreeChart chart = ChartFactory.createPieChart3D(title, dataset);
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(0);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}
	
}
